package models

// -- imports --
// -- end --

type GenderType int

const (
	Male GenderType = iota
	FEMALE
	PRIVATE
)
