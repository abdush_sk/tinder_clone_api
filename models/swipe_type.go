package models

type SwipeType int

const (
	LIKE SwipeType = iota
	UNLIKE
)
