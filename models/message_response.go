package models

type MessageListResponse struct {
	Code   int        `json:"code" bson:"code"`
	Error  string     `json:"error,omitempty" bson:"error"`
	Result []*Message `json:"result,omitempty" bson:"result"`
}
