package models

type UserListResponse struct {
	Code   int     `json:"code" bson:"code"`
	Error  string  `json:"error,omitempty" bson:"error"`
	Result []*User `json:"result,omitempty" bson:"result"`
}
