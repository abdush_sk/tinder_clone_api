package models

type Message struct {
	Deleted   bool   `json:"deleted,omitempty" bson:"deleted"`
	EpochTime int64  `json:"epoch_time,omitempty" bson:"epoch_time"`
	From      string `json:"from,omitempty" bson:"from"`
	To        string `json:"to,omitempty" bson:"to"`
	Message   string `json:"message,omitempty" bson:"message"`
}
