package models

type StatusResponse struct {
	Code  int    `json:"code" bson:"code"`
	Error string `json:"error,omitempty" bson:"error"`
}

func (s StatusResponse) GetCode() int {
	return s.Code
}
