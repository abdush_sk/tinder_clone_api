package models

type PassionType int

const (
	Running PassionType = iota
	Karoke
	Golf
	Potterhead
	Vlogging
	Cycling
	StandUpComedy
	Astrology
	Sushi
	Cricket
	Brunch
	Surfing
	Poetry
	Athlete
	Party
	Fashion
	DogLover
	Baking
	Anime
	Yoga
	Coffee
	Physics
	Chemistry
	Musician
	Foodie
)
