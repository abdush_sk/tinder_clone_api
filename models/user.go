package models

import (
	"time"
)

type User struct {
	Id           string        `json:"id" bson:"_id,omitempty"`
	Name         string        `json:"name,omitempty" bson:"name"`
	Password     string        `json:"password" bson:"password,omitempty"`
	Phone        string        `json:"phone,omitempty" bson:"phone"`
	Registered   bool          `json:"registered,omitempty" bson:"registered"`
	BirthDate    *time.Time    `json:"birth_date,omitempty" bson:"birth_date"`
	Gender       GenderType    `json:"gender,omitempty" bson:"gender"`
	Images       []string      `json:"images,omitempty" bson:"images"`
	PassionTypes []PassionType `json:"passions,omitempty" bson:"passions"`
	Interested   GenderType    `json:"interested,omitempty" bson:"interested"`
	School       string        `json:"school,omitempty" bson:"school"`
	Like         []string      `json:"like,omitempty" bson:"like"`
	UnLike       []string      `json:"unlike,omitempty" bson:"unlike"`
	Age          int           `json:"age,omitempty" bson:"age"`
	Lat          float64       `json:"lat,omitempty" bson:"lat"`
	Long         float64       `json:"long,omitempty" bson:"long"`
	ETime        int64         `json:"etime,omitempty" bson:"etime"`
}

// func (v *Validator) GameFromBody() *Game {
// 	b, err := ioutil.ReadAll(v.r.Body)
// 	if err != nil {
// 		v.Error("body", err.Error())
// 		return nil
// 	}

// 	ret := &Game{}
// 	err = json.Unmarshal(b, ret)
// 	if err != nil {
// 		v.Error("body", err.Error())
// 		return nil
// 	}

// 	if !ret.Valid() {
// 		v.Error("body", "Invalid Game")
// 		return nil
// 	}

// 	return ret
// }
