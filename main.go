package main

import (
	"context"
	"fmt"
	"os"
	"time"
	"tinder_clone/operations"

	"net/http"

	"go.uber.org/zap"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/jessevdk/go-flags"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	// -- imports --
	// -- end --
)

type MongoOptions struct {
	Uri      string `long:"uri" description:"Enter Mongo URI to connect to" default:"mongodb://localhost:27017"`
	Database string `long:"database" description:"Which MongoDatabse to connect to" default:"tinder"`
}

type Options struct {
	Host  string        `short:"h" long:"host" description:"What host" default:""`
	Port  int           `short:"p" long:"port" description:"Enter port to run the server on" default:"8000"`
	Mongo *MongoOptions `group:"mongo" namespace:"mongo"`
}

func main() {
	var opts Options
	_, err := flags.ParseArgs(&opts, os.Args)
	if err != nil {
		panic("Could not parse command line args")
	}
	var logger *zap.Logger

	var config zap.Config
	config = zap.NewDevelopmentConfig()

	config.EncoderConfig.TimeKey = ""
	config.OutputPaths = []string{"stdout"}

	logger, _ = config.Build()

	defer logger.Sync()

	// -- before-setup --
	//Random comment

	// -- end --

	mongoDb, err := mongoDB(opts.Mongo)
	if err != nil {
		panic("Could not initialize mongo:" + err.Error())
	}

	r := mux.NewRouter()
	sugar := "tinder"
	// r.Handle("/transactions/{id}/fake", operations.GenerateFakeTxn(opts.Sugar, mongoDb, fbApp, logger)).Methods("POST")
	r.Handle("/register", operations.Register(mongoDb, logger, sugar)).Methods("POST")
	r.Handle("/register-complete", operations.RegisterCompleted(mongoDb, logger, sugar)).Methods("POST")
	r.Handle("/login", operations.Login(mongoDb, logger, sugar)).Methods("POST")
	r.Handle("/user/{id}", operations.RegisterCompleted(mongoDb, logger, sugar)).Methods("POST")
	r.Handle("/me", operations.Me(mongoDb, logger, sugar)).Methods("POST")
	r.Handle("/get-users", operations.GetUsers(mongoDb, logger, sugar)).Methods("POST")
	r.Handle("/swipe", operations.Swipe(mongoDb, logger, sugar)).Methods("POST")
	r.Handle("/get-matches", operations.GetMatches(mongoDb, logger, sugar)).Methods("GET")
	r.Handle("/send-message", operations.SendMessage(mongoDb, logger, sugar)).Methods("POST")
	r.Handle("/get-messages", operations.GetMessages(mongoDb, logger, sugar)).Methods("POST")
	r.Handle("/update", operations.UpdateProfile(mongoDb, logger, sugar)).Methods("POST")

	// r.Handle("/me", operations.Me(mongoDb, logger, sugar)).Methods("GET")

	allowedMethods := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS", "DELETE"})
	allowedHeaders := handlers.AllowedHeaders([]string{"jwt", "build", "Content-Type", "content-type"})
	exposedHeaders := handlers.ExposedHeaders([]string{"jwt", "build", "Content-Type", "content-type"})

	logger.Info("Starting Server")
	http.ListenAndServe(fmt.Sprintf("%s:%d", opts.Host, opts.Port), handlers.CORS(exposedHeaders, allowedHeaders, allowedMethods)(r))
}

func mongoDB(opts *MongoOptions) (*mongo.Database, error) {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	mongoClient, err := mongo.Connect(ctx, options.Client().ApplyURI(opts.Uri))
	if err != nil {
		return nil, err
	}

	ctx, _ = context.WithTimeout(context.Background(), 10*time.Second)
	err = mongoClient.Ping(ctx, readpref.Primary())
	if err != nil {
		return nil, err
	}

	return mongoClient.Database(opts.Database), nil
}

// -- extra --
// -- end --
