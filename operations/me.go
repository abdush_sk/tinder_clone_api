package operations

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"tinder_clone/models"

	jwt "github.com/dgrijalva/jwt-go"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
)

func Me(mongoDb *mongo.Database, logger *zap.Logger, sugar string) http.Handler {
	oLog := logger.With(zap.String("op", "me"))
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()
		lat2 := r.FormValue("lat")
		long2 := r.FormValue("long")
		lat, _ := strconv.ParseFloat(lat2, 5)
		long, _ := strconv.ParseFloat(long2, 5)
		tokenString := r.Header.Get("jwt")
		claims := jwt.MapClaims{}
		_, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
			return []byte(sugar), nil
		})
		// ... error handling
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		// do something with decoded claims
		for key, val := range claims {
			fmt.Printf("Key: %v, value: %v\n", key, val)
		}
		userId := claims["user_id"]
		var user models.User
		res := mongoDb.Collection("users").FindOneAndUpdate(r.Context(), bson.M{
			"_id": userId,
		}, bson.M{"$set": bson.M{"lat": lat, "long": long, "epoch": time.Now().Unix()}})
		err = res.Decode(&user)
		if err != nil {
			oLog.Error("Not Found", zap.Error(err))
			w.WriteHeader(http.StatusNotFound)
			return
		}
		oLog.Info("me called")
		user.ETime = time.Now().Unix()
		JSON(&models.UserResponse{
			Code:   200,
			Result: &user,
		}, w)

	})
}
