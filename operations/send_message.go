package operations

import (
	"context"
	"encoding/json"
	"net/http"
	"time"
	"tinder_clone/models"

	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
)

func SendMessage(mongoDb *mongo.Database, logger *zap.Logger, sugar string) http.Handler {
	oLog := logger.With(zap.String("op", "register"))

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		m := models.Message{}
		err := json.NewDecoder(r.Body).Decode(&m)
		if err != nil {
			logger.Info("asd", zap.Any("error", err))
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		m.EpochTime = time.Now().Unix()

		_, err = mongoDb.Collection("messages").InsertOne(context.TODO(), m)
		if err != nil {
			w.WriteHeader(http.StatusConflict)
			return
		}
		oLog.Info("operation completed")
		JSON(models.StatusResponse{
			Code: 200,
		}, w)

	})
}
