package operations

import (
	"context"
	"net/http"
	"tinder_clone/models"

	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
)

func Register(mongoDb *mongo.Database, logger *zap.Logger, sugar string) http.Handler {
	oLog := logger.With(zap.String("op", "register"))

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()
		userName := r.FormValue("username")
		pass := r.FormValue("password")

		if userName == "" || pass == "" {
			JSON(models.StatusResponse{
				Code:  http.StatusBadRequest,
				Error: "Username and pass empty",
			}, w)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		pass = CreateHash(pass)
		var u models.User = models.User{
			Id:       userName,
			Password: pass,
		}
		u.Registered = false
		_, err := mongoDb.Collection("users").InsertOne(context.TODO(), u)
		if err != nil {
			w.WriteHeader(http.StatusConflict)
			JSON(models.StatusResponse{
				Code:  http.StatusConflict,
				Error: "Already exists, try a different username",
			}, w)
			return
		}
		oLog.Info("operation completed")
		JSON(models.StatusResponse{
			Code: 200,
		}, w)

		//tokens
		// token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		// 	"user_id":   u.Id,
		// 	"user_name": u.Name,
		// })
		// signedToken, err := token.SignedString([]byte(sugar))
		// if err != nil {
		// 	w.WriteHeader(http.StatusInternalServerError)
		// 	return
		// }
		// w.Header().Set("jwt", signedToken)
		// oLog.Info("register")
	})
}
