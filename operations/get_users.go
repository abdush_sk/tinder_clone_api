package operations

import (
	"fmt"
	"net/http"
	"time"
	"tinder_clone/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
)

func GetUsers(mongoDb *mongo.Database, logger *zap.Logger, sugar string) http.Handler {
	oLog := logger.With(zap.String("op", "register"))

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// r.ParseForm()
		userId := r.FormValue("user_id")
		var u models.User
		res2 := mongoDb.Collection("users").FindOneAndUpdate(r.Context(), bson.M{
			"_id": userId,
		}, bson.M{"$set": bson.M{"epoch": time.Now().Unix()}})

		err := res2.Decode(&u)
		if err != nil {
			oLog.Error("Not Found", zap.Error(err))
			w.WriteHeader(http.StatusNotFound)
			return
		}
		fmt.Println(u.Id)
		var users []*models.User
		res, err := mongoDb.Collection("users").Find(r.Context(), bson.M{
			"$and": []bson.M{ // you can try this in []interface
				bson.M{"registered": true},
			},
		})
		if err != nil {
			return
		}
		if err := res.All(r.Context(), &users); err != nil {
			oLog.Error("Decode Failed", zap.Error(err))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		result := []*models.User{}
	outer:
		for _, v := range users {
			if v.Id == u.Id {
				continue
			}
			for _, z := range u.Like {
				if z == v.Id {
					continue outer
				}
			}
			for _, z := range u.UnLike {
				if z == v.Id {
					continue outer
				}
			}
			result = append(result, v)
		}
		oLog.Info("get-users done ", zap.Any("length", len(result)))
		JSON(&models.UserListResponse{
			Code:   200,
			Result: result,
		}, w)
	})
}
