package operations

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"net/http"
)

func JSON(o interface{}, w http.ResponseWriter) {
	b, err := json.Marshal(o)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Write(b)
}

func CreateHash(key string) string {
	hasher := md5.New()
	hasher.Write([]byte(key))
	return hex.EncodeToString(hasher.Sum(nil))
}
