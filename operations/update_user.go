package operations

import (
	"net/http"
	"strconv"
	"tinder_clone/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
)

func UpdateProfile(mongoDb *mongo.Database, logger *zap.Logger, sugar string) http.Handler {
	oLog := logger.With(zap.String("op", "login"))

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()
		name := r.FormValue("name")
		id := r.FormValue("userId")
		ageTemp := r.FormValue("age")
		age, _ := strconv.Atoi(ageTemp)
		update := bson.M{}
		if name != "" {
			update["name"] = name
		}
		if age != 0 {
			update["age"] = age
		}

		mongoDb.Collection("users").UpdateOne(r.Context(), bson.M{"_id": id}, bson.M{"$set": update})
		oLog.Info("user updated")
		JSON(&models.StatusResponse{
			Code: 200,
		}, w)
	})
}
