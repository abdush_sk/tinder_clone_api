package operations

import (
	"fmt"
	"net/http"
	"time"
	"tinder_clone/models"

	jwt "github.com/dgrijalva/jwt-go"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
)

func GetMatches(mongoDb *mongo.Database, logger *zap.Logger, sugar string) http.Handler {
	oLog := logger.With(zap.String("op", "register"))

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// r.ParseForm()
		tokenString := r.Header.Get("jwt")
		claims := jwt.MapClaims{}
		_, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
			return []byte(sugar), nil
		})

		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		userId := claims["user_id"]
		var u models.User
		res2 := mongoDb.Collection("users").FindOneAndUpdate(r.Context(), bson.M{
			"_id": userId,
		}, bson.M{"$set": bson.M{"epoch": time.Now().Unix()}})

		err = res2.Decode(&u)
		if err != nil {
			oLog.Error("Not Found", zap.Error(err))
			w.WriteHeader(http.StatusNotFound)
			return
		}
		fmt.Println(u.Id)

		var users []*models.User
		res, err := mongoDb.Collection("users").Find(r.Context(), bson.M{
			"$and": []bson.M{ // you can try this in []interface
				bson.M{"registered": true},
			},
		})
		if err != nil {
			return
		}
		if err := res.All(r.Context(), &users); err != nil {
			oLog.Error("Decode Failed", zap.Error(err))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		var result []*models.User

	outer:
		for _, v := range users {
			for _, ul := range v.Like {
				if ul == u.Id {
					for _, ml := range u.Like {
						if ml == v.Id {
							result = append(result, v)
							continue outer
						}
					}
				}
			}
		}

		oLog.Info("get-matched done ", zap.Any("length", len(result)))
		JSON(&models.UserListResponse{
			Code:   200,
			Result: result,
		}, w)
	})
}
