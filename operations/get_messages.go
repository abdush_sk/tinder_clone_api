package operations

import (
	"net/http"

	"tinder_clone/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
)

func GetMessages(mongoDb *mongo.Database, logger *zap.Logger, sugar string) http.Handler {
	oLog := logger.With(zap.String("op", "get-messages"))
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()
		user1 := r.FormValue("user1")
		user2 := r.FormValue("user2")
		var messages []*models.Message

		res, err := mongoDb.Collection("messages").Find(r.Context(), bson.M{
			"$and": []bson.M{
				{"from": bson.M{"$in": []string{user1, user2}}},
				{"to": bson.M{"$in": []string{user1, user2}}},
			},
		})
		if err != nil {
			return
		}
		if err := res.All(r.Context(), &messages); err != nil {
			oLog.Error("Decode Failed", zap.Error(err))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		for i := 1; i < len(messages); i += 1 {
			currentValue := messages[i]
			j := i - 1
			for i > 0 && messages[j].EpochTime > currentValue.EpochTime {
				messages[j+1] = messages[j]
				j = j - 1
			}
			messages[j+1] = currentValue
		}

		JSON(&models.MessageListResponse{
			Code:   200,
			Result: messages,
		}, w)
	})
}
