package operations

import (
	"net/http"
	"strconv"
	"tinder_clone/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
)

func Swipe(mongoDb *mongo.Database, logger *zap.Logger, sugar string) http.Handler {
	oLog := logger.With(zap.String("op", "login"))

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()
		id := r.FormValue("user_id")
		forUser := r.FormValue("for_user_id")
		swipeType := r.FormValue("type")

		tp, err := strconv.Atoi(swipeType)
		if err != nil {
			oLog.Info("wrong type", zap.Any("swipetpye", swipeType))
			w.WriteHeader(300)
			return
		}
		swipeTp := models.SwipeType(tp)
		if id == forUser {
			w.WriteHeader(http.StatusInternalServerError)

			return
		}
		var user models.User
		if err = mongoDb.Collection("users").FindOne(r.Context(), bson.M{"_id": id}).Decode(&user); err != nil {
			if err == mongo.ErrNoDocuments {
				oLog.Error("User Not Found")
				w.WriteHeader(http.StatusInternalServerError)

				JSON(&models.UserResponse{
					Code:  http.StatusBadRequest,
					Error: "User Not Found",
				}, w)
			} else {
				w.WriteHeader(http.StatusInternalServerError)
			}
			return
		}
		var finalUsersLike []string
		var finalUsersUnlike []string
		for _, v := range user.Like {
			if v == forUser {
				continue
			}
			finalUsersLike = append(finalUsersLike, v)

		}
		for _, v := range user.UnLike {
			if v == forUser {
				continue
			}
			finalUsersUnlike = append(finalUsersUnlike, v)
		}

		if swipeTp == models.LIKE {
			finalUsersLike = append(finalUsersLike, forUser)
		}
		if swipeTp == models.UNLIKE {
			finalUsersUnlike = append(finalUsersUnlike, forUser)
		}
		mongoDb.Collection("users").UpdateOne(r.Context(), bson.M{"_id": id}, bson.M{"$set": bson.M{"unlike": finalUsersUnlike, "like": finalUsersLike}})
		oLog.Info("swipe done")
	})
}
