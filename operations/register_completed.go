package operations

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"
	"tinder_clone/models"

	jwt "github.com/dgrijalva/jwt-go"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
)

func RegisterCompleted(mongoDb *mongo.Database, logger *zap.Logger, sugar string) http.Handler {
	oLog := logger.With(zap.String("op", "register"))

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		u := models.User{}
		err := json.NewDecoder(r.Body).Decode(&u)
		if err != nil {
			logger.Info("asd", zap.Any("error", err))
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		u.ETime = time.Now().Unix()
		id := u.Id
		u.Id = ""
		u.Registered = true
		k, err := mongoDb.Collection("users").UpdateOne(r.Context(), bson.M{"_id": id}, bson.M{"$set": u})
		if err != nil {
			oLog.Info("asd", zap.Any("error", err))
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		fmt.Println(k.MatchedCount)
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"user_id":   id,
			"user_name": u.Name,
		})
		signedToken, err := token.SignedString([]byte(sugar))
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Header().Set("jwt", signedToken)
		oLog.Info("register")
	})
}
