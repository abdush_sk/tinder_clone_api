package operations

import (
	"net/http"
	"strconv"
	"time"
	"tinder_clone/models"

	jwt "github.com/dgrijalva/jwt-go"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
)

func Login(mongoDb *mongo.Database, logger *zap.Logger, sugar string) http.Handler {
	oLog := logger.With(zap.String("op", "login"))

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()
		userName := r.FormValue("username")
		pass := r.FormValue("password")
		lat2 := r.FormValue("lat")
		long2 := r.FormValue("long")
		lat, _ := strconv.ParseFloat(lat2, 5)
		long, _ := strconv.ParseFloat(long2, 5)

		var u models.User
		if err := mongoDb.Collection("users").FindOne(r.Context(), bson.M{"_id": userName}).Decode(&u); err != nil {
			if err == mongo.ErrNoDocuments {
				oLog.Error("User Not Found")
				JSON(&models.UserResponse{
					Code:  http.StatusBadRequest,
					Error: "User Not Found",
				}, w)
			} else {
				w.WriteHeader(http.StatusInternalServerError)
			}
			return
		}
		pass = CreateHash(pass)

		if u.Password != pass {
			JSON(&models.UserResponse{
				Code:  401,
				Error: "Wrong password",
			}, w)
			w.WriteHeader(http.StatusForbidden)
			return
		}

		if !u.Registered {
			JSON(&models.UserResponse{
				Code:   200,
				Result: &u,
			}, w)
			return
		}
		mongoDb.Collection("users").UpdateOne(r.Context(), bson.M{"_id": userName}, bson.M{"$set": bson.M{"lat": lat, "long": long, "epoch": time.Now().Unix()}})
		//tokens
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"user_id":   u.Id,
			"user_name": u.Name,
		})
		signedToken, err := token.SignedString([]byte(sugar))
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		u.Lat = lat
		u.Lat = long
		u.ETime = time.Now().Unix()
		w.Header().Set("jwt", signedToken)
		oLog.Info("Logged In")
		JSON(&models.UserResponse{
			Code:   200,
			Result: &u,
		}, w)
	})
}
